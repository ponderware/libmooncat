(ns libmooncat.ethereum.http)

(def request-nonce (atom 1000))

(defn- next-request-nonce! []
  (swap! request-nonce inc))

(defn prom-json-post-request [url json-body]
  (new js/Promise
       (fn [resolve reject]
         (let [xhr (new js/XMLHttpRequest)]
           (set! (.-onload xhr)
                 (fn [_]
                   (if (and (< (.-status xhr) 300) (<= 200 (.-status xhr)))
                     (resolve (.-responseText xhr))
                     (reject (.-status xhr)))))
           (.open xhr "POST" url)
           (.setRequestHeader xhr "Content-Type" "application/json")
           (.send xhr json-body)))))

(defn make-contract-request [rpc-endpoint contract-address data]
  (new
   js/Promise
   (fn [resolve _]
     (let [raw-prom (prom-json-post-request
                     rpc-endpoint
                     (js/JSON.stringify #js{"method" "eth_call"
                                            "id" (next-request-nonce!)
                                            "jsonrpc" "2.0"
                                            "params" #js[#js{"to" contract-address
                                                             "data" data}
                                                         "latest"]}))]
       (-> raw-prom
           (.then (fn [response-text]
                    (let [body-json (js->clj (js/JSON.parse response-text))
                          rpc-result (get body-json "result")
                          rpc-error (get body-json "error")]
                      (resolve
                       {:success (not rpc-error)
                        :http-status 200
                        :rpc-error rpc-error
                        :result rpc-result}))))
           (.catch (fn [status]
                     (resolve
                      {:success false
                       :http-status status}))))))))
