(ns libmooncat.ethereum.macros-cljs)

(defmacro with-result-data [[data-sym rpc-result] & body]
  `(let [res# ~rpc-result]
     (.then
      res#
      (fn [result#]
        (new js/Promise (fn [resolve# reject#]
                          (if (:success result#)
                            (let [~data-sym (:result result#)]
                              (resolve# (do ~@body)))
                            (reject# result#))))))))
