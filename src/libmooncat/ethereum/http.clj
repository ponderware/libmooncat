(ns libmooncat.ethereum.http
  (:require
   [cheshire.core :as json]
   [org.httpkit.client :as http]))

(def request-nonce (atom 1000))

(defn- next-request-nonce! []
  (swap! request-nonce inc))

(defn make-contract-request [rpc-endpoint contract-address data]
  (let [raw @(http/post rpc-endpoint {:headers {"Content-Type" "application/json"}
                                      :body (json/generate-string
                                             {"method" "eth_call"
                                              "id" (next-request-nonce!)
                                              "jsonrpc" "2.0"
                                              "params" [{"to" contract-address
                                                         "data" data}
                                                        "latest"]})})
        request-error (when (:error raw)
                     (:error raw))

        http-status (when-not request-error (:status raw))
        body-json (when (and (not request-error) (< http-status 300) (<= 200 http-status))
                    (json/parse-string (:body raw)))
        result (when body-json
                 (get body-json "result"))
        rpc-error (when body-json
                    (get body-json "error"))]
    {:success (and (not request-error) (not rpc-error))
     :request-error request-error
     :http-status http-status
     :rpc-error rpc-error
     :result result}))
