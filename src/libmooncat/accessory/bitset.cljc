(ns libmooncat.accessory.bitset
  (:require
   [clojure.string :as str]
   [libmooncat.util :as util]))

(defprotocol BitSet
  (get-index [BitSet index])
  (set-index! [BitSet index])
  (clear-index! [BitSet index])
  (to-solidity [BitSet])
  (to-indexes [BitSet]))

(deftype MoonCatBitSet [data]
  BitSet
  (get-index [this index]
    (let [byte-index (quot index 8)
          bit-index (- 7 (rem index 8))]
      (bit-test (aget data byte-index) bit-index)))
  (set-index! [this index]
    (let [byte-index (quot index 8)
          bit-index (- 7 (rem index 8))]
      (aset data byte-index (int (bit-set (aget data byte-index) bit-index)))
      this))
  (clear-index! [this index]
    (let [byte-index (quot index 8)
          bit-index (- 7 (rem index 8))]
      (aset data byte-index (int (bit-clear (aget data byte-index) bit-index)))
      this))
  (to-solidity [this]
    (->> data
         (reduce (fn [hex-vec b]
                   (conj hex-vec (util/byte->hex b)))
                 [])
         (partition 32)
         (map #(str "0x" (str/join %)))
         vec))
  (to-indexes [this]
    (persistent!
     (areduce data i res (transient [])
              (let [byte (aget data i)]
                (if (zero? byte)
                  res
                  (let [res (if (bit-test byte 7) (conj! res (+ 0 (* i 8))) res)
                        res (if (bit-test byte 6) (conj! res (+ 1 (* i 8))) res)
                        res (if (bit-test byte 5) (conj! res (+ 2 (* i 8))) res)
                        res (if (bit-test byte 4) (conj! res (+ 3 (* i 8))) res)
                        res (if (bit-test byte 3) (conj! res (+ 4 (* i 8))) res)
                        res (if (bit-test byte 2) (conj! res (+ 5 (* i 8))) res)
                        res (if (bit-test byte 1) (conj! res (+ 6 (* i 8))) res)
                        res (if (bit-test byte 0) (conj! res (+ 7 (* i 8))) res)]
                    res)))))))

(defn from-solidity [data]
  (let [bytes (->> data
                   (map util/clean-hex-prefix)
                   (str/join)
                   (re-seq #".{2}")
                   (map util/hex->int))
        arr #?(:clj (int-array bytes)
               :cljs (.from js/Uint8Array bytes))]
    (MoonCatBitSet. arr)))

(defn mooncat-bitset []
  (MoonCatBitSet.
   #?(:clj (int-array 3200)
      :cljs (new js/Uint8Array 3200))))

(defn indexes->bitset [indexes]
  (reduce
   (fn [mcbs index]
     (set-index! mcbs index))
   (mooncat-bitset)
   indexes))

(defn test-solidity-bit [sol index]
  (let [word-index (quot index 256)
        bit-in-word-index (rem index 256)
        nibble-index (quot bit-in-word-index 4)
        bit-index (- 3 (rem bit-in-word-index 4))
        word (nth sol word-index)
        nibble-hex (subs (util/clean-hex-prefix word) nibble-index (inc nibble-index))
        nibble (util/hex->int nibble-hex)]
    (bit-test nibble bit-index)))

(defn update-solidity-bit [sol index new-state]
  (let [word-index (quot index 256)
        bit-in-word-index (rem index 256)
        nibble-index (quot bit-in-word-index 4)
        bit-index (- 3 (rem bit-in-word-index 4))
        word (nth sol word-index)
        clean-word (util/clean-hex-prefix word)
        nibble-hex (subs clean-word nibble-index (inc nibble-index))
        nibble (util/hex->int nibble-hex)
        new-nibble (if new-state
                     (bit-set nibble bit-index)
                     (bit-clear nibble bit-index))
        new-nibble-hex #?(:clj  (Integer/toString new-nibble 16)
                          :cljs (.toString new-nibble 16))
        new-word (str "0x" (subs clean-word 0 nibble-index) new-nibble-hex (subs clean-word (inc nibble-index)))]
    (assoc sol word-index new-word)))
