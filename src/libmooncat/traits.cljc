(ns libmooncat.traits
  (:require
   [clojure.string :as str]
   [libmooncat.data.color :as color]
   [libmooncat.data.resources :as resources :refer [cat-id->rescue-order]]
   [libmooncat.util :as util]))

;;;; Base

(defn get-base-traits [cat-id]
  (let [cat-id (util/clean-hex-prefix cat-id)
        k-int (util/hex->int (subs cat-id 2 4))
        k-bin (util/byte->bin k-int)
        genesis (str/starts-with? cat-id "f")
        is-pale (= "1" (subs k-bin 0 1))
        is-pale (if genesis
                  (or (and (even? k-int) is-pale) (and (odd? k-int) (not is-pale)))
                  is-pale)
        hue-value (color/cat-id->hue cat-id)]
    {:cat-id (str "0x" cat-id)
     :genesis genesis
     :pale is-pale
     :is-pale is-pale
     :facing (if (= (subs k-bin 1 2) "0")
               :left
               :right)
     :expression (condp = (subs k-bin 2 4)
                   "00" :smiling
                   "01" :grumpy
                   "10" :pouting
                   "11" :shy)
     :pattern (condp = (subs k-bin 4 6)
                "00" :pure
                "01" :tabby
                "10" :spotted
                "11" :tortie)
     :pose (condp = (subs k-bin 6 8)
             "00" :standing
             "01" :sleeping
             "10" :pouncing
             "11" :stalking)
     :k-bin k-bin
     :k-int k-int
     :hue-value hue-value
     :hue (color/hue->color-key hue-value)
     :glow (color/cat-id->glow cat-id)}))

;;;; Extended

(defn get-rescue-year [rescue-index]
  (condp >= rescue-index
    3364 2017
    5683 2018
    5754 2019
    5757 2020
    2021))

(defn get-genesis-group [rescue-index]
  (condp >= rescue-index
    99 1
    539 2
    1117 3
    1764 4
    2379 5
    2891 6))

(defn get-mint-details [cat-id]
  (let [genesis (re-find #"^0xff" cat-id)]
    (if-let [rescue-index (cat-id->rescue-order cat-id)]
      (if genesis
        {:rescue-index rescue-index
         :rescue-year (get-rescue-year rescue-index)
         :classification :genesis
         :genesis-group (get-genesis-group rescue-index)}
        {:rescue-index rescue-index
         :rescue-year (get-rescue-year rescue-index)
         :classification :rescue
         :rescued-by (resources/rescue-order->rescuer rescue-index)})
      (cond
        (not genesis) {:classification :lunar}
        (re-matches #"0xff[0-9a-f][0-9a-f]000ca7" cat-id) {:classification :hero}
        :else {:classification :invalid}))))

(defn- add-groupings [{:as traits :keys [rescue-index k-bin]}]
  (if rescue-index
    (let [palette-key (color/rescue-order->palette-key rescue-index)
          facing-bit (subs k-bin 1 2)
          expression-bits (subs k-bin 2 4)
          pattern-bits (subs k-bin 4 6)
          pose-bits (subs k-bin 6 8)
          litter-id (str palette-key (util/bin->hex (str pattern-bits "000000")))
          twin-id   (str palette-key (util/bin->hex (str pattern-bits expression-bits "0000")))
          mirror-id (str palette-key (util/bin->hex (str pattern-bits expression-bits pose-bits "00")))
          clone-id  (str palette-key (util/bin->hex (str pattern-bits expression-bits pose-bits "0" facing-bit)))

          litter-set (resources/get-group :litter litter-id rescue-index)
          litter-size (count litter-set)
          only-child (= litter-size 1)

          twin-set (resources/get-group :twin twin-id rescue-index)
          twin-set-size (count twin-set)
          has-twins (< 1 twin-set-size)

          mirror-set (resources/get-group :mirror mirror-id rescue-index)
          mirror-set-size (count mirror-set)
          has-mirrors (< 1 mirror-set-size)

          clone-set (resources/get-group :clone clone-id rescue-index)
          clone-set-size (count clone-set)
          has-clones (< 1 clone-set-size)]
      (assoc traits
             :litter-id litter-id
             :only-child only-child
             :litter-size litter-size
             :litter litter-set

             :twin-id twin-id
             :has-twins has-twins
             :twin-set-size twin-set-size
             :twin-set twin-set

             :mirror-id mirror-id
             :has-mirrors has-mirrors
             :mirror-set-size mirror-set-size
             :mirror-set mirror-set

             :clone-id clone-id
             :has-clones has-clones
             :clone-set-size clone-set-size
             :clone-set clone-set))


    traits))

(defn get-extended-traits [cat-id]
  (add-groupings
   (merge (get-base-traits cat-id)
          (get-mint-details cat-id))))


;;;; ERC 721


(defn- title-case [s]
  (let [s (cond
            (nil? s) ""
            (vector? s) (str/join " " (map title-case s))
            (string? s) (str/trim s)
            (keyword? s) (name s)
            :else (str/trim (str s)))]
    (if (str/blank? s)
      ""
      (str/trim (str/join " " (map #(str (str/upper-case (subs % 0 1)) (subs % 1)) (remove str/blank? (str/split s #"[\s\-]+"))))))))

(defn- generate-erc721-description [{:keys [is-pale pattern hue expression classification rescue-year hue-value]}]
  (let [normal-type (or (= classification :rescue) (= classification :lunar))
        hue (if (and is-pale normal-type)
              (str (title-case hue) "/" (title-case (color/hue->color-key (mod (+ hue-value 320) 360))))
              hue)]
    (str
     (title-case [(when (= pattern :pure) pattern)
                  (when (and is-pale normal-type) :pale)
                  hue
                  expression
                  (when (not= pattern :pure) pattern)
                  (when (not= classification :rescue) classification)
                  "MoonCat"]
                 ))))

(defn- erc721-str-trait [trait-type value]
  (when-not (nil? value)
    {:trait_type (title-case trait-type)
     :value (title-case value)}))

(defn- erc721-ranking-trait [trait-type value max-value]
  (when-not (nil? value)
    {:trait_type (title-case trait-type)
     :value value
     :max_value max-value}))

(defn- erc721-bool-trait [trait-type value]
  (when-not (nil? value)
    {:trait_type (str (title-case trait-type) "?")
     :value (if value "Yes" "No")}))

(defn- generate-erc721-attributes [{:keys [classification rescue-year cat-id rescue-index rescued-by
                                    is-pale hue pattern expression pose facing
                                    only-child has-twins has-mirrors has-clones]}]
  (filterv
   #(not (nil? %))
   [(erc721-str-trait "MoonCat Id" cat-id)
    (erc721-ranking-trait :rescue-index rescue-index (dec resources/+total-mooncats+))
    (erc721-str-trait :classification classification)
    (erc721-str-trait :rescue-year rescue-year)
    (erc721-str-trait :coat [(when is-pale :pale) hue pattern])
    (erc721-str-trait :expression expression)
    (erc721-str-trait :pose pose)
    #_(str-trait :rescued-by rescued-by)
    (erc721-bool-trait :only-child only-child)
    (erc721-bool-trait :has-twins has-twins)
    (erc721-bool-trait :has-mirrors has-mirrors)
    (erc721-bool-trait :has-clones has-clones)]))

(defn get-erc721-traits [cat-id]
  (let [traits (get-extended-traits cat-id)]
    {:description (generate-erc721-description traits)
     :background_color "000000"
     :attributes (generate-erc721-attributes traits)
     :details traits}))

;;;; Generators

(defn- hue->rgb-hex [hue]
  (str/join (map util/byte->hex (color/hue->rgb hue))))

(defn generate-cat-id [{:keys [hue hue-value expression pattern pose facing pale is-pale] :or {expression :smiling pattern :pure pose :standing facing :left}}]
  (let [hue (or hue-value hue -1)
        hue (if (number? hue)
              hue
              (color/color-key->hue hue))
        pale (if (nil? pale) is-pale pale)
        k (str
           (cond
             (== (int hue) -2) (if (#{:sleeping :stalking} pose) "0" "1")
             (== (int hue) -1) (if (#{:sleeping :stalking} pose) "1" "0")
             pale "1"
             :else "0")
           (if (= facing :left) "0" "1")
           (case expression
             :smiling "00"
             :grumpy  "01"
             :pouting "10"
             :shy     "11"
             "00")
           (case pattern
             :pure    "00"
             :tabby   "01"
             :spotted "10"
             :tortie "11"
             "00")
           (case pose
             :standing "00"
             :sleeping "01"
             :pouncing "10"
             :stalking "11"
             "00"))
        k (util/byte->hex (util/bin->int k))
        prefix (if (< hue 0)
                 "ff"
                 "00")
        rgb (if (< hue 0)
              "000ca7"
              (hue->rgb-hex hue))]
    (str "0x" prefix k rgb)))


(def +hues+ (vec (range -2 360)))
(def +expressions+ [:smiling :grumpy :pouting :shy])
(def +patterns+ [:pure :tabby :spotted :tortie])
(def +poses+ [:standing :sleeping :pouncing :stalking])
(def +facings+ [:left :right])
(def +pales+ [true false])
(defn pick-random [v]
  (let [i #?(:cljs (int (js/Math.floor (* (js/Math.random) (count v))))
             :clj (int (Math/floor (* (Math/random) (count v)))))]
    (v i)))

(defn generate-random-cat-id
  ([]
   (generate-cat-id
    {:hue (pick-random +hues+)
     :expression (pick-random +expressions+)
     :pattern (pick-random +patterns+)
     :pose (pick-random +poses+)
     :facing (pick-random +facings+)
     :pale (pick-random +pales+)}))
  ([{:keys [hues expressions patterns poses facings pales
            hue expression pattern pose facing pale]
     :or {hues +hues+
          expressions +expressions+
          patterns +patterns+
          poses +poses+
          facings +facings+
          pales +pales+}}]
   (generate-cat-id
    {:hue (or hue (pick-random hues))
     :expression (or expression (pick-random expressions))
     :pattern (or pattern (pick-random patterns))
     :pose (or pose (pick-random poses))
     :facing (or facing (pick-random facings))
     :pale (if (boolean? pale)
             pale
             (pick-random pales))})))
