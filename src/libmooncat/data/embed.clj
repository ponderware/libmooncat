(ns libmooncat.data.embed
  (:require
   [clojure.java.io :as io]))

(defn embed [resource-path]
  (read-string (slurp (io/resource resource-path))))
