(ns libmooncat.image.pixels
  (:require
   [libmooncat.util :as util]
   #?(:cljs [cljsjs.pako :as pako]))
  #?(:clj
     (:import
      [java.util.zip Deflater Inflater])))

(def ^:private debug-print-chars [" " "*" "-" "@" "#" "$" "%" "." "_"])
(def ^:dynamic *debug-print* false)

(def ^:const ^:private +largest-integer+ #?(:clj Long/MAX_VALUE :cljs js/Number.MAX_SAFE_INTEGER))
(def ^:const +max-dim+ 128)

(defn minimize-image
  "Takes a `pixels` vector which contains vectors of triples
  `[x y i]` with x-coordinate `x` y-coordinate `y` and
  palette-index `i`

  e.g.
  `[[4 4 1]
    [3 1 2]
    ...]`

  It computes the minimum bounding rectangle and evaluates
  to a `pipeline` map containing:

    :pixels - the supplied pixels vector
    :width - the (minimized) image width
    :height - the (minimized) image height
    :offset-x - translation offset for raw `x` coord to minimized `x` coord
    :offset-y - translation offset for raw `y` coord to minimized `y` coord
  "
  [pixels]
  (if (empty? pixels)
    {:pixels []
     :offset-x 0 :offset-y 0
     :width 0 :height 0}
    (let [[min-x max-x min-y max-y]
          (reduce (fn [[min-x max-x min-y max-y] [x y _]]
                    [(min min-x x)
                     (max max-x x)
                     (min min-y y)
                     (max max-y y)])
                  [+largest-integer+ 0 +largest-integer+ 0]
                  pixels)
          width (inc (- max-x min-x))
          height (inc (- max-y min-y))]
      #_(assert (and (< (+ min-x width) +max-dim+)
                   (< (+ min-y height) +max-dim+)) "image is too large")
      {:pixels pixels
       :offset-x min-x
       :offset-y min-y
       :width (inc (- max-x min-x))
       :height (inc (- max-y min-y))})))

(defn- map-palette
  "Takes a `pipleline` map and creates a mapping between
  the raw palette-index and the final palette-index.

  The mapping is determined by ordering the palette indexes
  in the `pixels` vec from most-frequently to least-frequently used.
  When two palette-indexes appear with equal frequency, the one
  which appears first in the image (scanned left->right top->bottom)
  is assigned the lower mapping number

  Evalutes to the `pipeline` map with an added `:palette-mapping` key
  which maps raw pallete indexes to their ordered values"
  [{:as pipeline :keys [pixels]} remap]
  (if remap
    (let [mapping-data (reduce (fn [mapping-data [x y palette-index]]
                                 (let [[cnt min-y min-x] (mapping-data palette-index)
                                          cnt (if cnt (dec cnt) -1) ;; negative counts are used for easy sorting
                                          min-y (if min-y (min min-y y) y)
                                          min-x (if min-x (min min-x x) x)]
                                      (assoc mapping-data palette-index [cnt min-y min-x])))
                                  {}
                                  pixels)
          sorted-mapping-data (sort-by val mapping-data)
          palette-mapping (reduce (fn [palette-mapping [original-palette-index _]]
                                    (assoc palette-mapping original-palette-index (count palette-mapping)))
                                  {0 0}
                                  sorted-mapping-data)]
      (assoc pipeline :palette-mapping palette-mapping))

    (assoc pipeline :palette-mapping (reduce (fn [m [_ _ pi]]
                                               (assoc m pi pi))
                                             {0 0}
                                             pixels))))

(defn- generate-png-pixel-bytes
  "Takes a `pipeline` map of and transforms the `pixels` vec into
  a full Width x Height byte array of mapped palette indexes.
  Each row of pixels is prepended with a `0` filter byte.

  Evaluates to the `pipeline` map with an added `:pixel-byte-array` key"
  [{:as pipeline :keys [pixels offset-x offset-y width height palette-mapping]}]
  (let [width (long width)
        height (long height)
        offset-x (long offset-x)
        offset-y (long offset-y)
        row-width (inc width)
        len (* row-width height) ;; each row begins with a filter byte (0)
        nibble-vector (reduce (fn [nibbles [raw-x raw-y palette-index]]
                                (let [raw-x (long raw-x) raw-y (long raw-y)]
                                  (if (and (number? palette-index) (pos? (long palette-index)))
                                    (let [x (inc (- raw-x offset-x))
                                          y (- raw-y offset-y)]
                                      (assoc nibbles (+ x (* y row-width)) (byte palette-index)))
                                    nibbles)))
                              (vec (repeat len 0))
                              pixels)
        ba #?(:clj (byte-array len)
              :cljs (js/Uint8Array. len))]
    (when *debug-print*
      (let [pvec (mapv #(get debug-print-chars (palette-mapping %)) nibble-vector)]
        (println " ") (println " ")
        (dotimes [j height]
          (println (subvec pvec (* j row-width) (+ (* j row-width) row-width))))
        (println " ") (println " ")))
    (dotimes [i len]
      (aset ba i (byte (palette-mapping (nibble-vector i))))
      #_(aset ba i (byte (nibble-vector i)))) ;; <- ignores palette mapping
    (assoc pipeline :pixel-byte-array ba)))


(defn scale-pixel-bytes
  [{:as pipeline :keys [pixel-byte-array width height]} scale-factor]
  (if (and (int? scale-factor) (pos? scale-factor))
    (let [^"[B" pixel-byte-array pixel-byte-array
          width (long width)
          height (long height)
          scale-factor (long scale-factor)
          old-row-width (inc width)
          new-width (* width scale-factor)
          new-height (* height scale-factor)
          new-row-width (inc new-width)
          scaled-len (* new-row-width new-height)
          scaled-ba #?(:clj (byte-array scaled-len)
                       :cljs (js/Uint8Array. scaled-len))]
      (dotimes [old-index (alength pixel-byte-array)]
        (let [old-x (rem old-index old-row-width)
              old-y (quot old-index old-row-width)]
          (when-not (zero? old-x)
            (let [new-x (inc (* (dec old-x) scale-factor))
                  new-y (* old-y scale-factor new-row-width)
                  new-index (+ new-x new-y)]
              (dotimes [offset-y scale-factor]
                (dotimes [offset-x scale-factor]
                  (aset scaled-ba (+ (+ new-index offset-x) (* offset-y new-row-width)) (aget pixel-byte-array old-index))))))))
      (assoc pipeline
             :width (* width scale-factor)
             :height (* height scale-factor)
             :pixel-byte-array scaled-ba))
    pipeline))


(defn- deflate-pixel-bytes
  "Takes a `pipeline` map and compresses the contained `:pixel-byte-array`
  evaluating to the `pipeline` with an added `:compressed-pixel-byte-array`
  key"
  [{:as pipeline :keys [pixel-byte-array]}]
  #?(:clj
     (let [pixel-byte-array ^"[B" pixel-byte-array
           compressor (doto (Deflater. (int 9) false)
                        (.setInput pixel-byte-array)
                        (.finish))
           output (byte-array (+ 16 (alength pixel-byte-array)))
           compressed-length (.deflate compressor output)
           result (byte-array compressed-length)]
       (.end compressor)
       (System/arraycopy output 0 result 0 compressed-length)
       (assoc pipeline :idat result))
     :cljs
     (assoc pipeline :idat (.deflate js/pako pixel-byte-array #js{"level" 9}))))

(defn- add-idat-hex
  "Takes the a `pipeline` map and add the `:idat-hex` key whose value is the
  hex representation of the `:idat` byte-array (from the `pipeline` map)"
  [{:as pipeline :keys [idat]}]
  (assoc pipeline :idat-hex (util/byte-array->hex idat)))

(defn gen-pixel-data
  "Takes a `pixels` vector which contains vectors of triples
  `[x y i]` with x-coordinate `x` y-coordinate `y` and
  palette-index `i`

  e.g.
  `[[4 4 1]
    [3 1 2]
    ...]`


  It computes the minimum bounding rectangle and evaluates
  to a map containing:
    :pixels - the supplied `pixels` vec
    :width - the (minimized) image width
    :height - the (minimized) image height
    :offset-x - translation offset for raw `x` coord to minimized `x` coord
    :offset-y - translation offset for raw `y` coord to minimized `y` coord
    :palette-mapping - mapping of compressed and sorted palette-indexes
                        `{old-index0 new-index0, old-index1 new-index1, ...}`
    :idat - a zlib compressed PNG IDAT compatible pixel data array
    :idat-hex - the PNG IDAT data in hexidecimal
"
  ([pixels]
   (gen-pixel-data pixels false nil))
  ([pixels remap-palette scale-factor]
   (-> pixels
       minimize-image
       (map-palette remap-palette)
       generate-png-pixel-bytes
       (scale-pixel-bytes scale-factor)
       deflate-pixel-bytes
       add-idat-hex)))

;;;;

(defn- inflate-pixel-bytes
  "Takes a `pipeline` map and compresses the contained `:pixel-byte-array`
  evaluating to the `pipeline` with an added `:compressed-pixel-byte-array`
  key"
  [idat]
  #?(:clj
     (let [idat ^"[B" idat
           decompressor (doto (Inflater.)
                          (.setInput idat 0 (alength idat)))
           output (byte-array (* (inc +max-dim+) +max-dim+))
           decompressed-length (.inflate decompressor output)
           result (byte-array decompressed-length)]
       (.end decompressor)
       (System/arraycopy output 0 result 0 decompressed-length)
       result)
     :cljs
     (.inflate js/pako idat)))

(defn read-pixel-data
  "Takes a `pixel-data` map which must contain the keys:
    :width - width of the image
    :height - height of the image
    :idat-hex - hex encoded (compressed) IDAT byte array

  and the optional keys:
    :offset-x - added to `x` coordinates
    :offset-y - added to `y` coordinates
    :palette-fn - run on the palette index
    :mirror - if true flip pixels horizontally

  Evaluates to a `pixels` vec which contains vectors of triples
  `[x y i]` with x-coordinate `x` y-coordinate `y` and
  palette-index `i`"
  [{:as pixel-data
    :keys [width height idat-hex idat offset-x offset-y palette-fn mirror]
    :or {offset-x 0 offset-y 0 palette-fn identity}}]
  (let [row-width (inc (long width))
        height (long height)
        idat (util/hex->byte-array (or idat-hex idat))
        pixel-bytes (inflate-pixel-bytes idat)
        offset-x (or offset-x 0)
        offset-y (or offset-y 0)
        ;;offset-x (if mirror (- +max-dim+ offset-x width) offset-x)
        ]
    (vec
     (keep-indexed (fn [i byte]
                     (when-not (or (zero? byte) (zero? (rem i row-width)))
                       (let [byte (palette-fn (int (if (neg? byte) (+ byte 256) byte)))
                             x (if mirror
                                 (+ offset-x (- width (+ (rem i row-width))))
                                 (+ (rem i row-width) offset-x -1))
                             y (+ (quot i row-width) offset-y)]
                         [x y byte])))
                   pixel-bytes))))
