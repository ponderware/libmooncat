(ns libmooncat.image.mooncattemplate
  (:require
   [libmooncat.traits :as traits]
   [libmooncat.data.color :as color]
   [libmooncat.data.resources :refer [designs]]
   [libmooncat.util :as util]))

(defn- get-offsets [pose facing]
  (case [pose facing]
    [:standing :left] [-5.5 -7.5]
    [:sleeping :left] [-6.5 -7.5]
    [:pouncing :left] [-6.5 -7.5]
    [:stalking :left] [-5.5 -13.5]
    [:standing :right] [-15.5 -7.5]
    [:sleeping :right] [-13.5 -7.5]
    [:pouncing :right] [-10.5 -7.5]
    [:stalking :right] [-14.5 -13.5]))


(defn get-template-data [cat-id]
  (let [cat-id (util/clean-hex-prefix cat-id)
        k-int (util/hex->int (subs cat-id 2 4))
        k-bin (util/byte->bin k-int)
        facing (if (= "0" (subs k-bin 1 2)) :left :right)
        pose (case (subs k-bin 6 8)
               "00" :standing
               "01" :sleeping
               "10" :pouncing
               "11" :stalking)]
    {:k-int k-int
     :facing facing
     :pose pose}))

(defn get-template [cat-id]
  (let [{:keys [k-int facing pose]} (get-template-data cat-id)
        {:keys [width height raw-pixels]} (designs (mod k-int 128))
        pixels (vec (keep-indexed
                     (fn [i pi]
                       (when (< 0 pi)
                         [(rem i width)
                          (quot i width)
                          pi]))
                     raw-pixels))
        [ox oy] (get-offsets pose facing)]
    {:width width
     :height height
     :register-x ox
     :register-y oy
     :pixels pixels}))

(defn get-head-mask [pose facing]
  (case [pose facing]
    [:standing :left] [0 0]
    [:sleeping :left] [1 0]
    [:pouncing :left] [1 0]
    [:stalking :left] [0 6]
    [:standing :right] [10 0]
    [:sleeping :right] [8 0]
    [:pouncing :right] [5 0]
    [:stalking :right] [9 6]))

(defn head-offset [pose-index right]
  (case [pose-index right]
    [0 false] [0 0]
    [1 false] [-2 0]
    [2 false] [-2 0]
    [3 false] [0 -12]
    [0 true] [-20 0]
    [1 true] [-16 0]
    [2 true] [-10 0]
    [3 true] [-18 -12]))


(def ^:private removed-head-pixels #{[0 9]
                                     [0 10]
                                     [1 10]
                                     [10 9]
                                     [9 10]
                                     [10 10]})

(defn get-head-template [cat-id]
  (let [{:keys [k-int facing pose]} (get-template-data cat-id)
        {:keys [width height raw-pixels]} (designs (mod k-int 128))
        pixels (vec (keep-indexed
                     (fn [i pi]
                       (when (< 0 pi)
                         [(rem i width)
                          (quot i width)
                          pi]))
                     raw-pixels))
        [ox oy] (get-offsets pose facing)
        [mx my] (get-head-mask pose facing)
        pixels (->> pixels
                    (filter (fn [[x y]]
                              (and (>= x mx) (<= x (+ mx 10))
                                   (>= y my) (<= y (+ my 10)))))
                    (map (fn [[x y c]] [(- x mx) (- y my) c]))
                    (filter (fn [[x y]] (not (removed-head-pixels [x y]))))
                    vec)]
    {:width 11
     :height 11
     :register-x ox
     :register-y oy
     :pixels pixels}))



(defn mooncat-image-vector [cat-id]
  (let [props (traits/get-base-traits cat-id)
        k-int (:k-int props)
        {:keys [width height raw-pixels]} (designs (mod k-int 128))
        palette (vec (map-indexed (fn [i [r g b]]
                                    (when (pos? i)
                                      (str "#" (util/byte->hex r) (util/byte->hex g) (util/byte->hex b))))
                                  (color/cat-id->palette cat-id)))
        image-vector (mapv palette raw-pixels)
        [ox oy] (get-offsets (:pose props) (:facing props))]
    {:width width
     :height height
     :register-x ox
     :register-y oy
     :image-vector image-vector}))
