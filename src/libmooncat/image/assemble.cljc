(ns libmooncat.image.assemble
  (:require
   [clojure.string :as str]
   [libmooncat.accessory.palette :as palette]
   [libmooncat.accessory.meta :as meta]
   [libmooncat.data.color :as color]
   [libmooncat.image.mooncattemplate :as templates]
   [libmooncat.image.pixels :as pixels]
   #?(:clj [libmooncat.image.generator.buffered-image :refer [generate-buffered-image]]
      :cljs [libmooncat.image.generator.browser-png-uri :refer [generate-png-uri]])))


(def ^:dynamic *glow-size* 20)
(def ^:dynamic *glow-opacity* 0.8)

(defn gen-mooncat-layer [cat-id full-size head-only]
  (if full-size
    (let [{:keys [width height register-x register-y pixels]} (if head-only
                                                                (templates/get-head-template cat-id)
                                                                (templates/get-template cat-id))

          left (long (+ (/ pixels/+max-dim+ 2) (* register-x 2)))
          top (long (+ (/ pixels/+max-dim+ 2) (* register-y 2)))
          scaled-pixels (reduce (fn [spc [x y i]]
                                  (let [color (+ i palette/+mooncat-border-color-index+ -1)
                                        x (+ (* x 2) left)
                                        y (+ (* y 2) top)]
                                    (conj spc
                                          [x            y  color]
                                          [(inc x)      y  color]
                                          [x       (inc y) color]
                                          [(inc x) (inc y) color])))
                                []
                                pixels)]
      {:top 0
       :left 0
       :bottom 128
       :right 128
       :z-index 0
       :pixels scaled-pixels})


    (let [{:keys [width height register-x register-y pixels]} (if head-only
                                                                (templates/get-head-template cat-id)
                                                                (templates/get-template cat-id))
          left (long (+ (/ pixels/+max-dim+ 2) (* register-x 2)))
          top (long (+ (/ pixels/+max-dim+ 2) (* register-y 2)))
          scaled-pixels (reduce (fn [spc [x y i]]
                                  (let [color (+ i palette/+mooncat-border-color-index+ -1)
                                        x (+ (* x 2))
                                        y (+ (* y 2))]
                                    (conj spc
                                          [x            y  color]
                                          [(inc x)      y  color]
                                          [x       (inc y) color]
                                          [(inc x) (inc y) color])))
                                []
                                pixels)]
      {:top top
       :left left
       :bottom (+ top (* height 2))
       :right (+ left (* width 2))
       :z-index 0
       :pixels scaled-pixels})))

(defn- parse-meta [{:keys [meta background mirror-accessory mirror-placement]}]
  (let [parsed-meta (when (int? meta) (meta/from-metabyte meta))
        background (if (nil? background)
                     (:background parsed-meta)
                     background)
        mirror-accessory (if (nil? mirror-accessory)
                           (:mirror-accessory parsed-meta)
                           mirror-accessory)
        mirror-placement (if (nil? mirror-placement)
                           (:mirror-placement parsed-meta)
                           mirror-placement)]
    {:background background
     :mirror-accessory mirror-accessory
     :mirror-placement mirror-placement}))


(defn- gen-accessory-layer [cat-position-index cat-facing-right full-size head-only
                            {:as accessory :keys [positions palettes width height idat palette-index z-index]}]
  (let [{:keys [background mirror-accessory mirror-placement]} (parse-meta accessory)
        [offset-x offset-y] (positions cat-position-index)
        [mx my] (templates/head-offset cat-position-index cat-facing-right)
        offset-x (if head-only
                   (if (and cat-facing-right mirror-placement)
                     (- offset-x mx)
                     (+ offset-x mx))
                   offset-x)
        offset-y (if head-only (+ offset-y my) offset-y)
        palette (get palettes palette-index)]
    (when-not (or (zero? z-index) (zero? width) (zero? height)
                  (nil? z-index) (nil? width) (nil? height)
                  (<= pixels/+max-dim+ offset-x) (<= pixels/+max-dim+ offset-y)
                  (nil? palette))
      (let [
            palette (into [nil] palette)
            z-index (if background (- z-index) z-index)
            offset-x (if (and cat-facing-right mirror-placement)
                       (- pixels/+max-dim+ offset-x width)
                       offset-x)
            pixels (pixels/read-pixel-data {:width width
                                            :height height
                                            :idat-hex idat
                                            :palette-fn palette
                                            :mirror (and cat-facing-right mirror-accessory)
                                            :offset-x (when full-size offset-x)
                                            :offset-y (when full-size offset-y)
                                            })]
        {:top (if full-size 0 offset-y)
         :left (if full-size 0 offset-x)
         :bottom (if full-size 128 (+ offset-y height))
         :right (if full-size 128 (+ offset-x width))
         :z-index z-index
         :pixels pixels}))))

(defn- process-dimensions [layers]
  (loop [[l & ls] layers
         fg []
         bg []
         top pixels/+max-dim+
         bottom 0
         left pixels/+max-dim+
         right 0]
    (if (nil? l)
      {:foreground-layers fg
       :background-layers bg
       :image-top top
       ;;:image-bottom bottom
       :image-left left
       ;;:image-right right
       :image-height (- bottom top)
       :image-width (- right left)}
      (if (neg? (:z-index l))
        (recur ls
               fg
               (conj bg l)
               (min top (:top l))
               (max bottom (:bottom l))
               (min left (:left l))
               (max right (:right l)))
        (recur ls
               (conj fg l)
               bg
               (min top (:top l))
               (max bottom (:bottom l))
               (min left (:left l))
               (max right (:right l)))))))

(defn- collect-pixels [image-top image-left get-color #_gpap [glow-pixels all-pixels] {:as layer :keys [top left pixels]}]
  (let [end (count pixels)]
    (loop [i 0
           glow-pixels glow-pixels
           all-pixels all-pixels]
      (if (== i end)
        [glow-pixels all-pixels]
        (let [[x y c] (pixels i)
              new-pixel [(- (+ x left) image-left) (- (+ y top) image-top) (get-color c)]]
          (if (== c palette/+mooncat-border-color-index+)
            (recur (inc i) (conj glow-pixels new-pixel) (conj all-pixels new-pixel))
            (recur (inc i) glow-pixels (conj all-pixels new-pixel))))))))

(defn- assemble-all-layers [cat-id layers glow]
  (let [image-layers (->> layers
                          (remove nil?)
                          (sort-by :z-index))

        glow-color-rgb  (color/cat-id->glow cat-id)
        glow-color-html (str "rgba(" (str/join "," glow-color-rgb) "," *glow-opacity* ")")

        {:keys [foreground-layers background-layers
                image-top image-left
                image-width image-height]} (process-dimensions image-layers)

        get-color #?(:cljs (mapv :html (palette/get-full-palette cat-id))
                     :clj (mapv :rgba (palette/get-full-palette cat-id)))

        collect (partial collect-pixels image-top image-left get-color)

        [background-glow background-pixels] (reduce collect [#{} []] background-layers)
        [foreground-glow foreground-pixels] (reduce collect [#{} []] foreground-layers)]
    (if (or (<= image-height 0) (<= image-width 0))
      {:width 0
       :height 0
       :data nil}
      {:width image-width
       :height image-height
       :data {:glow-color #?(:clj (conj glow-color-rgb (int (* 255 *glow-opacity*)))
                             :cljs glow-color-html)
              :glow-size *glow-size*
              :background-glow (when glow background-glow)
              :background-pixels background-pixels
              :foreground-glow (when glow foreground-glow)
              :foreground-pixels foreground-pixels}})))

(defn- prep-editor-accessory [{:as accessory :keys [idat idat-hex palette palettes palette-index]}]
  (let [palettes (cond
                   palettes palettes
                   palette [(vec (rest palette))]
                   :else (throw (ex-info "no palettes" {})))]
    (assoc accessory
           :palettes palettes
           :idat (or idat idat-hex)
           :palette-index (or palette-index 0))))

(defn- assemble-layers [cat-id accessories draw-cat glow full-size head-only]
  (let [props (templates/get-template-data cat-id)
        facing-right (= (:facing props) :right)
        position-index (case (:pose props)
                         :standing 0
                         :sleeping 1
                         :pouncing 2
                         :stalking 3)
        accessories (map prep-editor-accessory accessories)
        accessory-layers (map (partial gen-accessory-layer position-index facing-right full-size head-only) accessories)
        cat-layer (when draw-cat (gen-mooncat-layer cat-id full-size head-only))]
    (assemble-all-layers cat-id (conj accessory-layers cat-layer) glow)))

(defn generate [cat-id accessories {:keys [scale padding background-color full-size no-cat glow head-only] :or {scale 5 glow true}}]
  (let [{:keys [width height data]} (assemble-layers cat-id accessories (not no-cat) glow full-size head-only)
        scale (or scale 1)
        padding (or padding (* scale 3))]
    #?(:cljs (generate-png-uri width height data scale padding background-color)
       :clj (generate-buffered-image width height data scale padding background-color))
    ))

(defn generate-with-dimensions [cat-id accessories {:keys [scale padding background-color full-size no-cat glow head-only] :or {scale 5 glow true}}]
  (let [{:keys [width height data]} (assemble-layers cat-id accessories (not no-cat) glow full-size head-only)
        scale (or scale 1)
        padding (or padding (* scale 3))
        img #?(:cljs (generate-png-uri width height data scale padding background-color)
               :clj (generate-buffered-image width height data scale padding background-color))]
    {:img img
     :width (+ (* width scale) (* 2 padding))
     :height (+ (* height scale) (* 2 padding))}))
