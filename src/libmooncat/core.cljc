(ns libmooncat.core
  (:require
   #?(:clj [clojure.java.io :as io])
   [clojure.string :as str]
   [libmooncat.accessory.bitset :as bitset]
   [libmooncat.accessory.palette :as palette]
   [libmooncat.accessory.meta :as accessory-meta]
   [libmooncat.data.color :as color]
   [libmooncat.filter :as filter]
   [libmooncat.image.assemble :as image]
   [libmooncat.image.pixels :as pixels]
   [libmooncat.image.mooncattemplate :as template]
   [libmooncat.data.contracts :as contracts]
   [libmooncat.data.resources :as resources]
   [libmooncat.ethereum.rpc :as rpc]
   [libmooncat.traits :as traits]
   [libmooncat.util :as util]))

(def +version+ "1.1.4")

(def +total-mooncats+ resources/+total-mooncats+)

;;;; Helpers

(defn parse-cat-id
  "takes a cat-id or rescue-order and turns it into a validated cat-id hex string
  or a map with details if input is invalid"
  [cat-id-or-rescue-order]
  (cond
    (int? cat-id-or-rescue-order) (if (and
                                       (< cat-id-or-rescue-order +total-mooncats+)
                                       (<= 0 cat-id-or-rescue-order))
                                    (resources/rescue-order->cat-id cat-id-or-rescue-order)
                                    {:classification :invalid :reason "rescue order out of bounds"})
    (string? cat-id-or-rescue-order) (if (re-matches #"^[0-9]+$" cat-id-or-rescue-order)
                                       (recur (util/parse-int cat-id-or-rescue-order))
                                       (let [s (-> cat-id-or-rescue-order
                                                   str/lower-case
                                                   util/add-hex-prefix)]
                                         (if (re-matches #"0xff[0-9a-f]{2}000ca7$|0x00[0-9a-f]{8}$" s)
                                           s
                                           {:classification :invalid :reason "malformed cat-id"})))
    :else {:classification :invalid :reason "unrecognized input"}))


;;;; Contracts

(defn get-contract-address
  "get the Ethereum address of the smart contract associated with the `contract-key`

  supported `contract-key`s are:

   :mooncatrescue - Original MoonCatRescue Contract
   :mooncatacclimator - Official ERC721 MoonCatAcclimator wrapper
   :mooncataccessories - Official MoonCat Accessories
   :deprecated-unofficial-mooncatwrapper - Deprecated Unofficial MoonCat Wrapper"
  [contract-key]
  (get-in contracts/all [contract-key :address] "invalid key"))


(defn get-contract-abi
  "get the JSON ABI of the smart contract associated with the `contract-key`

  supported `contract-key`s are:

   :mooncatrescue - Original MoonCatRescue Contract
   :mooncatacclimator - Official ERC721 MoonCatAcclimator wrapper
   :mooncataccessories - Official MoonCat Accessories
   :deprecated-unofficial-mooncatwrapper - Deprecated Unofficial MoonCat Wrapper"
  [contract-key]
  (get-in contracts/all [contract-key :abi] "invalid key"))

;;;; Blockchain Queries

(defn mooncat-contract-details
  "Fetches contract details map for a rescued mooncat from the supplied rpc-url.
  If the cat doesn't exist the result is nil.
  In clojurescript, evaluates to a Promise"
  [rpc-url cat-id-or-rescue-order]
  (let [cat-id (parse-cat-id cat-id-or-rescue-order)
        rescue-order (resources/cat-id->rescue-order cat-id)
        result (when (and (string? cat-id) rescue-order) (rpc/get-cat-contract-details rpc-url cat-id))]
    #?(:clj result
       :cljs (new js/Promise (fn [resolve reject]
                               (if result
                                 (resolve result)
                                 (reject nil)))))))

(defn acclimated?
  "Fetches acclimation status from the blockchain"
  [rpc-url cat-id-or-rescue-order]
  (let [cat-id (parse-cat-id cat-id-or-rescue-order)
        rescue-order (resources/cat-id->rescue-order cat-id)]
    (if (and (string? cat-id) rescue-order)
      (rpc/is-cat-acclimated rpc-url cat-id)
      #?(:clj nil
         :cljs (new js/Promise (fn [resolve reject] (resolve false)))))))

(defn get-total-accessories
  "Fetches total number of accessories in contract as a STRING"
  [rpc-url]
  (rpc/get-total-accessories rpc-url))

(defn get-accessory-palette-count
  "Fetches integer palette count - integer"
  [rpc-url accessory-id]
  (rpc/get-accessory-palette-count rpc-url accessory-id))

(defn get-accessory-palette
  "Fetches accessory palette - vector of integers"
  [rpc-url accessory-id palette-index]
  (rpc/get-accessory-palette rpc-url accessory-id palette-index))

(defn get-accessory-info
  "Fetches accessory info - map"
  [rpc-url accessory-id]
  (rpc/get-accessory-info rpc-url accessory-id))

(defn get-accessory-image-data
  "Fetches accessory image data - map"
  [rpc-url accessory-id]
  (rpc/get-accessory-image-data rpc-url accessory-id))

(defn get-accessory-eligible-list
  "Fetches accessory eligible list - map"
  [rpc-url accessory-id]
  (rpc/get-accessory-eligible-list rpc-url accessory-id))

(defn get-accessory
  "Fetches all data associated with an accessory as a map with the keys:

    :total-supply - integer
    :available-supply - integer
    :name - string
    :manager - address
    :price-wei - string
    :price-eth - string
    :available - boolean
    :availablePalettes - integer
    :palettes - vec of palettes
    :positions - vec of positions
    :meta - integer
    :width - integer
    :height - integer
    :idat - string
    :eligible-list - vector of 100 bytes32s"
  [rpc-url accessory-id]
  (rpc/get-accessory rpc-url accessory-id))

(defn accessory-exists?
  "Fetches if the idat of an accessory already exists in the smart contract"
  [rpc-url idat-hex]
  (rpc/is-accessory-unique? rpc-url idat-hex))

(defn get-total-managed-accesories
  "Fetches total number of accessories managed by address"
  [rpc-url manager-address]
  (rpc/get-total-managed-accessories rpc-url manager-address))

(defn get-managed-accessory-id-by-index
  "Fetches the accessory-id of a managed accessory"
  [rpc-url manager-address index]
  (rpc/get-managed-accessory-id-by-index rpc-url manager-address index))

(defn get-total-mooncat-accessories
  "Fetches the number of accessories owned by the MoonCat"
  [rpc-url rescue-order]
  (rpc/get-total-mooncat-accessories rpc-url rescue-order))

(defn get-mooncat-accessory
  "Fetches the MoonCat's owned accessory data as a map with the keys:

    :accessory-id
    :palette-index
    :z-index"
  [rpc-url rescue-order owned-accessory-index]
  (rpc/get-mooncat-accessory rpc-url rescue-order owned-accessory-index))

(defn get-drawable-mooncat-accessory
  "Fetches the MoonCat's owned accessory data and merges it with the accessory image data
  resulting in a map with the keys:

    :positions
    :palettes
    :meta
    :idat
    :width
    :height
    :z-index
    :palette-index"
  [rpc-url rescue-order owned-accessory-index]
  (rpc/get-drawable-mooncat-accessory rpc-url rescue-order owned-accessory-index))

;;;; Cat Properties

(defn get-cat-id
  "Gets the cat-id hex associated with a given rescue-order"
  [rescue-order]
  (resources/rescue-order->cat-id rescue-order))

(defn get-rescue-order
  "Gets the rescue-order of a cat-id hex (if one exists)"
  [cat-id]
  (resources/cat-id->rescue-order cat-id))

(defn random-cat-id
  "Generates a random cat-id hex. If a `constraints` map is supplied,
  the resulting id will conform to the given attributes. Each value
  in a `constraints` map should be a vector of permitted values.

  available constraints and their relevant values are:

   :hues [numbers [-2, 360) | color keywords]
   :expressions [:smiling :grumpy :pouting :shy])
   :patterns [:pure :tabby :spotted :tortie])
   :poses [:standing :sleeping :pouncing :stalking])
   :facings [:left :right])
   :pales [true false])"
  ([] (traits/generate-random-cat-id))
  ([constraints] (traits/generate-random-cat-id constraints)))

(defn generate-cat-id
  "Generates a cat-id hex with the provided `attributes` map.
  If an attribute is missing, the first value is the default.

  available attributes are:

   :hue - number in [-2, 360) | color keyword | color string
   :expression :smiling | :grumpy | :pouting | :shy
   :pattern - :pure | :tabby | :spotted | :tortie
   :pose - :standing | :sleeping | :pouncing | :stalking])
   :facing - :left | :right
   :pale - true | false"
  [attributes]
  (traits/generate-cat-id attributes))


(defn get-base-traits
  "Evaluates to a map of `base-traits` parsed from a cat-id hex.

  a `base-traits` map has the keys:

    :cat-id - cat-id hex
    :genesis - bool
    :pale - bool
    :facing - :left | :right
    :expression :smiling | :grumpy | :pouting | :shy
    :pattern - :pure | :tabby | :spotted | :tortie
    :pose - :standing | :sleeping | :pouncing | :stalking])

    :k-int - integer k-value
    :hue-value - number base hue
    :hue - keyword hue color
    :glow - vector of 3 ints representing glow color [r g b]"
  [cat-id]
  (traits/get-base-traits cat-id))

(defn get-extended-traits
  "Evaluates to a map of `extended-traits`"
  [cat-id]
  (traits/get-extended-traits cat-id))

(defn get-erc721-traits
  "Evaluates to a map of `erc-721-traits`"
  [cat-id]
  (traits/get-erc721-traits cat-id))

(defn get-traits
  "safely get traits by cat-id or rescue-order

  invalid input will return a map `{:classification :invalid :reason \"...\"}`

  valid `result-type` arguments are:
    :basic | \"basic\"
    :extended | \"extended\"
    :erc721 | \"erc721

  default result-type is `basic`"
  ([cat-id-or-rescue-order]
   (get-traits :basic cat-id-or-rescue-order))
  ([result-type cat-id-or-rescue-order]
   (let [parsed-id (parse-cat-id cat-id-or-rescue-order)]
     (if (string? parsed-id)
       (condp = result-type
         :basic (get-base-traits parsed-id)
         :extended (get-extended-traits parsed-id)
         :erc721 (get-erc721-traits parsed-id)
         "basic" (get-base-traits parsed-id)
         "extended" (get-extended-traits parsed-id)
         "erc721" (get-erc721-traits parsed-id)
         "ERC721" (get-erc721-traits parsed-id)
         {:classification :invalid :reason "unrecognized result type"})
       parsed-id))))

;;;; Image Generation

(defn generate-image
  "Takes a `cat-id` hex string, a vector of `accessories`, and an
  optional `options` map. Evaluates to a png data uri.

  Each accessory should be a map with the keys:

    :idat - string the idat image data
    :palettes - vector of palettes (palette is an 8 el vec of integer palette indexes)
    :z-index - integer layer distance from cat (+/- determined by `background`)
    :positions - 4 el vec of 2 el vecs with x&y offsets for each position
    :width - integer width of the accessory
    :height - integer height of the accessory
    :meta - integer metadata of the accessory

    :palette-index (optional) - which palette to use (default: 0)
    :background (optional) - overrides meta attribute
    :mirror-placement (optional) - overrides meta attribute
    :mirror-accessory (optional) - overrides meta attribute
    :palette (dev) - takes a 9 element array, drops first element and sets as palette 0

  The available options are:

    :glow - boolean, when set draws glow where applicable (default: true)
    :glow-size integer size of glow (default: 20)
    :glow-opacity float 0-1 indicating opacity of glow (default 0.80)
    :scale - integer number of drawn pixels per accessory pixel (default: 1)
    :padding - integer number of pixels to pad the drawn image (default: 3 * scale)
    :background-color - string background fill (default: transparent)
    :full-size - boolean, when set draws full 128x128 image instead of minimum
                 bounding image (default: false)
    :no-cat - boolean, when true cat image is not included in output
    :head-only - boolean, when true only draw the mooncat head (experimental)"
  ([cat-id]
   (generate-image cat-id nil nil))
  ([cat-id accessories]
   (generate-image cat-id accessories nil))
  ([cat-id accessories options]
   (binding [libmooncat.image.assemble/*glow-size* (or (:glow-size options) 20)
             libmooncat.image.assemble/*glow-opacity* (or (:glow-opacity options) 0.8)]
     (image/generate cat-id accessories options))))

(defn generate-image-with-dimensions
  "evaluates to map containing:

  :img - image
  :width - width in pixels
  :height - height in pixels

  see `generate-image`"
  ([cat-id]
   (generate-image-with-dimensions cat-id nil nil))
  ([cat-id accessories]
   (generate-image-with-dimensions cat-id accessories nil))
  ([cat-id accessories options]
   (binding [libmooncat.image.assemble/*glow-size* (or (:glow-size options) 20)
             libmooncat.image.assemble/*glow-opacity* (or (:glow-opacity options) 0.8)]
     (image/generate-with-dimensions cat-id accessories options))))

#?(:clj (defn save-image!
          "Saves generated png image to disk. See `(doc generate-image)`"
          [filename cat-id & [accessories options]]
          (libmooncat.image.generator.buffered-image/save-png!
           (generate-image cat-id accessories options)
           filename)))

(defn get-mooncat-image-vector
  "deprecated"
  [cat-id]
  (template/mooncat-image-vector cat-id))


;;;; Pixel Data Handling

(defn generate-image-data
  "Takes a vector of pixels in the form `[[x0 y0 p0] [x1 y1 p1]...]`
  and evaluates a map containing the keys:

    :pixels - the supplied `pixels` vec
    :width - the (minimized) image width
    :height - the (minimized) image height
    :offset-x - translation offset for raw `x` coord to minimized `x` coord
    :offset-y - translation offset for raw `y` coord to minimized `y` coord
    :palette-mapping - mapping of compressed and sorted palette-indexes
                        `{old-index0 new-index0, old-index1 new-index1, ...}`
    :idat - a zlib compressed PNG IDAT compatible pixel data array
    :idat-hex - the PNG IDAT data in hexidecimal

  If `remap-palette` is true, the palette order will sorted from most
  frequently used to least frequently used.

  If `scale-factor` is a positive integer, each pixel in the resulting image
  data will be made of `scale-factor` pixels."
  ([pixels]
   (pixels/gen-pixel-data pixels))
  ([pixels remap-palette scale-factor]
   (pixels/gen-pixel-data pixels remap-palette scale-factor)))

(defn read-image-data
  "Takes an `image-data` map which must contain the keys:
    :width - width of the image
    :height - height of the image
    :idat-hex - hex encoded (compressed) IDAT byte array

  and the optional keys:
    :offset-x - added to `x` coordinates
    :offset-y - added to `y` coordinates
    :palette-fn - run on the palette index
    :mirror - if true flip pixels horizontally

  Evaluates to a `pixels` vec which contains vectors of triples
  `[x y i]` with x-coordinate `x` y-coordinate `y` and
  palette-index `i`"
  [image-data]
  (pixels/read-pixel-data image-data))

(defn get-offsets
  "Takes a vector of pixels in the form `[[x0 y0 p0] [x1 y1 p1]...]`
  and evaluates to the offsets of the minimum bounding image on a
  128x128 image grid in the form `[offset-x offset-y]`"
  [pixels]
  (let [data (pixels/minimize-image pixels)]
    [(:offset-x data) (:offset-y data)]))

(defn get-max-offsets
  "Takes a vector of pixels in the form `[[x0 y0 p0] [x1 y1 p1]...]`
  and evaulates to the maximum offsets for the image to fit on the
  128x128 image grid in the form `[max-offset-x max-offset-y]`"
  [pixels]
  (let [data (pixels/minimize-image pixels)]
    [(- pixels/+max-dim+ (:width data))
     (- pixels/+max-dim+ (:height data))]))


;;;; Palettes/Colors

(def +accessory-static-palette+
  "a vector of static accessory colors maps. Each map has the keys:

    :id - index in vector
    :rgba - vector [uint8 r, uint8 g, uint8 b, uint8 a]
    :html - string 'rgba(...)'
    :title - string"
  palette/static-colors)

(def +accessory-static-color-rows+
  "a map of grouped palette colors which do not depend on a MoonCat"
  palette/static-color-rows)

(def +mooncat-border-color-index+
  "the global palette index which corresponds to the `glowing` border
  color of the MoonCat. The MoonCat palette begins at this index"
  palette/+mooncat-border-color-index+)

(defn get-full-accessory-palette
  "Get the full accessory palette including MoonCat specific colors"
  [cat-id]
  (palette/get-full-palette cat-id))

(defn get-mooncat-palette-row
  "Get the accessory palette row for a MoonCat"
  [cat-id]
  (palette/get-mooncat-palette-row cat-id))

;;;; Accessory

(defn generate-metabyte
  "Turns an `accessory-metadata` map into a byte.

  `accessory-metadata` keys are:

    :audience - 0 | 1 | 2 | 3
    :mirror-accessory - bool
    :mirror-placement - bool
    :background - bool"
  [accessory-metadata]
  (accessory-meta/to-metabyte accessory-metadata))

(defn parse-metabyte
  "Parses a `metabyte` integer into a map with the keys:
    :verified - bool
    :reserved - 0-4
    :audience - 0 | 1 | 2 | 3
    :mirror-accessory - bool
    :mirror-placement - bool
    :background - bool"
  [metabyte]
  (accessory-meta/from-metabyte metabyte))

(defn generate-eligible-list
  "Turns a sequence of rescue-orders into a vector of 100 hex-encoded bytes32
  values representing an eligible-list"
  [rescue-orders]
  (bitset/to-solidity (bitset/indexes->bitset rescue-orders)))

(defn parse-eligible-list
  "Turns a vector of 100 hex-encode bytes32 values representing an eligible-list
  into a vector of rescue-orders"
  [eligible-list]
  (let [rescue-orders (bitset/to-indexes (bitset/from-solidity eligible-list))]
    (if (bitset/test-solidity-bit eligible-list 25599)
      (subvec rescue-orders 0 (dec (count rescue-orders)))
      rescue-orders)))

(defn eligible-list-active?
  "check if an eligible-list is active"
  [eligible-list]
  (when eligible-list
    (bitset/test-solidity-bit eligible-list 25599)))

(defn activate-eligible-list
  "sets the `active` bit on an eligible-list"
  [eligible-list]
  (bitset/update-solidity-bit eligible-list 25599 true))

(defn deactivate-eligible-list
  "clears the `active` bit on an eligible-list"
  [eligible-list]
  (bitset/update-solidity-bit eligible-list 25599 false))

(defn eligible?
  "checks an eligible-list for a given rescue-order"
  [eligible-list rescue-order]
  (if (eligible-list-active? eligible-list)
    (bitset/test-solidity-bit eligible-list rescue-order)
    true))

;;;; Filters

(defn filter-rescue-orders
  "Takes one or multiple `filter` map and returns an array of rescue-orders.
   If multiple `filter` maps are supplied, the results will be combined as though with OR.

  Each filter can specify multiple criteria that the MoonCat must meet. The available properties (criteria) are:

   :id - (f string) | string | vector of strings
   :hue - (f float)
   :order - (f rescue-order) | integer | vector of rescue-orders
   :vintage -  integer | array of integers
   :genesis - boolean
   :pale - boolean
   :facing: :left | :right
   :expression :smiling | :grumpy | :pouting | shy | vector of expressions
   :pattern: :pure | :tabby | :spotted | :tortie | vector of patterns
   :pose :standing | :sleeping | :pouncing | :stalking | vector of poses
   :color :red | :orange | :yellow | :chartreuse | :green | ... | :fuchsia | vector of colors"
  [& filters]
  (filter/filter-mooncats filters))

;;;; Lootprints

(defn get-lootprint
  "gets data associated with a lootprint if it exists"
  [lootprint-id]
  (when-let [lootprint-data (resources/lootprints lootprint-id)]
    {:class (lootprint-data 0)
     :bays (lootprint-data 1)
     :color (lootprint-data 2)
     :name (lootprint-data 3)}))

;;;; Misc Dev

#?(:cljs
   (defn ^:export print-all-hues []
     (print
      (str/join "\n"
                (map color/cat-id->hue resources/rescue-order->cat-id)))))


#?(:cljs
   (defn ^:export print-all-palettes []
     (print
      (str/join "\n"
                (map color/cat-id->palette resources/rescue-order->cat-id)))))

#?(:cljs
   (defn ^:export print-all-palette-keys []
     (print
      (str/join "\n"
                (map #(str "\"" % "\"") (map color/cat-id->palette-key resources/rescue-order->cat-id))))))

#_(defn foo []
(generate-accessory-png
 "0x001400ff00"
 {:width 26
  :height 22
  :palette  [1 114 5 3 0 0 0 0]
  :idat "0x78dacd92e10e80200884e1e0fd9f392041ac65eb5fb7b9091f87ca2432e1264a3d2240d9a52e3eb7030216082f0a047856a2bc71f719d242ade0155926929d6f9016e24b5795bc63daca54ef0a0bcfb543731a22eb04c74909737cadddafd0c7bf71002f220371"}
 {:scale-factor 10
  :to-uri true
  })
)

;;;; JS File Generation

#?(:clj
   (defn -main [& args]
     (let [command (first args)]
       (condp = command
         "gen-readme"
         (do
           (println "Copying javascript README from resources/README-js.md into dist-js & dist-node")
           (io/copy (io/file (io/resource "README-js.md")) (io/file "dist-js/README.md"))
           (io/copy (io/file (io/resource "README-js.md")) (io/file "dist-node/README.md")))
         "gen-resources.cljs"
         (do
           (println "generating full src/libmooncat/data/resources.cljs")
           (spit "src/libmooncat/data/resources.cljs" (resources/gen-cljs-resources)))
         "gen-limited-resources.cljs"
         (do
           (println "generating limited src/libmooncat/data/resources.cljs (excludes rescuer, group, & lootprint data)")
           (spit "src/libmooncat/data/resources.cljs" (resources/gen-limited-cljs-resources)))
         (do
           (println "unrecognized command:" command)
           (println "available commands:")
           (println "gen-resources.cljs - generates full resources.cljs file")
           (println "gen-limited-resources.cljs - generates abbreviated resources.cljs (excludes rescuer, group & lootprint data)"))))))
