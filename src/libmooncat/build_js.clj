(ns libmooncat.build-js)

(defmacro browser-or-node [browser-expression node-expression]
  (if (= :nodejs (get-in @cljs.env/*compiler* [:options :target]))
    node-expression
    browser-expression))

(defn- nodejs-target?
  []
  (= :nodejs (get-in @cljs.env/*compiler* [:options :target])))


(defmacro node-code
  [& body]
  (when (nodejs-target?)
    `(do ~@body)))

(defmacro browser-code
  [& body]
  (when-not (nodejs-target?)
    `(do ~@body)))
